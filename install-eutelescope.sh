#!/usr/bin/env bash

# =============================================================================
# radial-install.sh
# Script to install Xiaocong Ai's radial EUTelescope on lxplus (CentOS 7)
#     based on /afs/cern.ch/work/x/xai/public/ilcsoft/test/README
#     changes timing parameter name for newer LCIO files and removes an unneccessary EUDAQ dependency
# usage: source radial-install.sh path/to/install/
#     uses current directory if none is specified
#     confirms directory before installing
#     takes about 80 minutes to complete
# =============================================================================

# =============================================================================
# Initialize install path

# set path to argument if there is one, pwd, otherwise
ilcinstalldir=${1-$(pwd)}

# convert install path to absolute path
if [ ! -d $ilcinstalldir ]; then
    mkdir $ilcinstalldir
	ilcinstalldir=$(realpath $ilcinstalldir)
	rmdir $ilcinstalldir
else
	ilcinstalldir=$(realpath $ilcinstalldir)
fi

# =============================================================================
# Set up environment
# this can cause problems if a conflicting environment has already been set up
# also root sets up its own gcc

printf "\nInstalling radial EUTelescope to ${ilcinstalldir}\n"

if [ -e "${ilcinstalldir}/eutel-git" ]; then
	printf "\tdeleting old ${ilcinstalldir}/eutel-git\n"
fi

if [ -e "${ilcinstalldir}/v01-17-05" ]; then
	printf "\tdeleting old ${ilcinstalldir}/v01-17-05\n"
fi

if [ -e "${ilcinstalldir}/setup-eutelescope.sh" ]; then
	printf "\tdeleting old ${ilcinstalldir}/setup-eutelescope.sh\n"
fi

printf "\tis this ok? (y/n) "

read answer

if [ "${answer::1}" = "y" ] || [ "${answer::1}" = "Y" ]
then
	printf "\nStarting installation...\n"
else
	printf "\nExiting\n\n"
	return
fi

rm -rf "${ilcinstalldir}/eutel-git"
rm -rf "${ilcinstalldir}/v01-17-05"
rm -rf "${ilcinstalldir}/setup-eutelescope.sh"

printf "\nSetting up environment...\n\n"

setupATLAS
lsetup git
lsetup "cmake 3.11.0"
lsetup "root 6.04.12-x86_64-slc6-gcc49-opt"

# =============================================================================
# Retrieve and build eutel-git and retrieve Xiaocong's branch

printf "\nRetrieving eutel-git...\n\n"

export ILCSOFT=$ilcinstalldir
mkdir -p $ILCSOFT
cd $ILCSOFT

git clone https://gitlab.cern.ch/atlas-itk-strip-testbeam/eutelescope.git $ILCSOFT/temp-repo
mv $ILCSOFT/temp-repo/eutel-git $ILCSOFT/eutel-git
rm -rf $ILCSOFT/temp-repo

printf "\nInstalling iLCSoft with eutel-git...\n\n"

# build iLCSoft/EUTelescope using eutel-git
cd $ILCSOFT/eutel-git
$ILCSOFT/eutel-git/ilcsoft-install -i $ILCSOFT/eutel-git/examples/eutelescope/release-standalone.cfg

printf "\nRetrieving Xiaocong's radial branch...\n\n"

# get Xiaocong's branch
cd $ILCSOFT/v01-17-05/Eutelescope/v1.0
git remote add xai https://github.com/XiaocongAi/eutelescope.git
git fetch xai
git checkout -b Radial-recon xai/Radial-recon

# =============================================================================
# A couple of small changes

# the name of the timing parameter in the DAQ is now PTDC_DUT
sed -i 's|\"PTDC.BIT\"|\"PTDC_DUT.BIT\"|g' $ILCSOFT/v01-17-05/Eutelescope/v1.0/src/EUTelOutputTTree.cc

# this EUDAQ dependency is unneccesary for reconstruction and causes issues
sed -i "s|:.*/v01-17-05/Eutelescope/v1.0/external/eudaq/v1.6-dev-end/lib/libNativeReader.so||g" $ILCSOFT/v01-17-05/Eutelescope/v1.0/build_env.sh

# =============================================================================
# Install

printf "\nCompleting install...\n\n"

# set up environment for installation
source $ILCSOFT/v01-17-05/ILCSoft.cmake.env.sh
source $ILCSOFT/v01-17-05/init_ilcsoft.sh # note that this changes ILCSOFT to $ILCSOFT/v01-17-05
source $ILCSOFT/Eutelescope/v1.0/build_env.sh

# install
cd $ILCSOFT/Eutelescope/v1.0/build
rm -rf $ILCSOFT/Eutelescope/v1.0/build/* # remove preexisting build files which create a problem
export COMPILER_LOC=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Gcc/gcc493_x86_64_slc6/slc6/x86_64-slc6-gcc49-opt/bin
cmake -DROOT_CONFIG_EXECUTABLE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.12-x86_64-slc6-gcc49-opt/bin/root-config -DEIGEN2_INCLUDE_DIR=/usr/include/eigen3 -DCMAKE_CXX_COMPILER=$COMPILER_LOC/g++ -DCMAKE_C_COMPILER=$COMPILER_LOC/gcc ..
make -j4 && make install -j4

printf "printf \"%snSetting up environment for radial EUTelescope...%sn%sn\"\n\n" "\\" "\\" "\\" > $ilcinstalldir/setup-eutelescope.sh
printf "setupATLAS\n" >> $ilcinstalldir/setup-eutelescope.sh
printf "lsetup \"root 6.04.12-x86_64-slc6-gcc49-opt\"\n\n" >> $ilcinstalldir/setup-eutelescope.sh
printf "export ILCSOFT=\"${ilcinstalldir}\"\n" >> $ilcinstalldir/setup-eutelescope.sh
printf "source \$ILCSOFT/v01-17-05/Eutelescope/v1.0/build_env.sh\n\n" >> $ilcinstalldir/setup-eutelescope.sh
printf "printf \"%snDone%sn%sn\"\n" "\\" "\\" "\\" >> $ilcinstalldir/setup-eutelescope.sh

cd $ilcinstalldir

printf "\nDone\n\n"
